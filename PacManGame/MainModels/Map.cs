﻿using PacManGame.EatableModels;
using PacManGame.MapBuilder;
using PacManGame.MapBuilder.XMLBuilder;
using System;
using System.Collections.Generic;

namespace PacManGame.MainModels
{
    public class Map
    {
        public object[,] GameMap { get; set; }
        public FoodContext GameFoodContext { get; private set; }

        public Map(FoodContext foodContext)
        {
            GameFoodContext = foodContext;
        }

        public void ReadMapFromTXT(string path)
        {
            var builder = new TXTBuilder(path);

            GameMap = builder.BuildMap(GameFoodContext);

        }

        public void ReadMapFromXML(string path, GameEngine gameEngine)
        {
            XMLMapBuilder xML = new XMLMapBuilder(gameEngine);
            xML.LoadSingle(path);
            GameMap = xML.BuildMap(gameEngine.GameFoodContext);
        }

        public void AddSomeEatable(IEatable eatable, int x, int y)
        {
            if (GameMap[x, y] is PointFood pointFood)
            {
                GameFoodContext.CountOfPoints--;
            }
            GameMap[x, y] = eatable;
        }

        public Position GetRandomEmptyPosition()
        {
            Random rand = new Random();
            int X = 0, Y = 0;

            while(true)
            {
                X = rand.Next(0, GameMap.GetLength(0));
                Y = rand.Next(0, GameMap.GetLength(1));

                // Bad system. Need to save all empty positions.
                if (GameMap[X, Y] is Empty && ((X != 8 || X != 9 || X != 10) && Y != 9))
                    break;
            }
            return new Position(X, Y);

        }
    }
}
