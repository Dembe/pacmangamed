﻿namespace PacManGame.MainModels
{
    public interface IVisited
    {
        bool IsWalkable { get; }
        /// <summary>
        /// Object visited by PacMan.
        /// </summary>
        /// <param name="pMan"></param>
        /// <returns></returns>
        bool Accept(PMan pMan);

        bool GetInto(FoodContext foodContext);
    }
}
