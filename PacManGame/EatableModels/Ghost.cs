﻿using PacManGame.AStar;
using PacManGame.GhostBehavior;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace PacManGame.EatableModels
{
    public class Ghost : Creature, IEatable
    {
        private readonly Map _pacManMap;
        private bool _isNeedToRespawn = false;
        private bool _isInsideInnerList = false;
        private List<MyPathNode> _path;
        // Need to save 2+ objects at 1 position on map.
        private readonly List<object> _innerList = new List<object>();

        public IBehavior Behavior { get; private set; }
        public IBehavior DefaultBehavior { get; private set; }
        public TeleportationPoint TpPoint { get; set; }

        public Position EndPosition { get; private set; }

        public bool IsFrightened { get; private set; }
        public bool IsDead { get; private  set; }
        public bool IsWalkable { get; private set; }
        public bool Active { get; set; }

        public string ImgUrl { get; private set; }

        public int Points { get; private set; }

        public event EventHandler<PositionEventArgs> PositionChanged;

        public Ghost(IBehavior behavior, Map currentMap, int X, int Y, string imgUrl, bool active)
        {
            Behavior = behavior;
            DefaultBehavior = behavior;
            Speed = 550;
            _pacManMap = currentMap;
            IsWalkable = true;

            Position position = new Position(X, Y);
            CurrentPosition = position;
            DefaltPosition = position;
            EndPosition = position;

            Active = active;

            ImgUrl = imgUrl;
        }

        private void GoToSpawn()
        {
            // Set shortes path from current position to default.
            Behavior = new DeadBehavior(_pacManMap, TpPoint);
            SetPath(DefaltPosition);
            _isNeedToRespawn = true;
        }

        private void SetCurrentPosition(int X, int Y)
        {
            var positionEventArgs = new PositionEventArgs(CurrentPosition.X, CurrentPosition.Y);
            CurrentPosition = new Position(X, Y);
            PositionChanged?.Invoke(this, positionEventArgs);
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public override bool Move()
        {
            if (_path == null || _path.Count == 0)
                return false;

            MyPathNode nextPosition = _path[0];
            _path.Remove(nextPosition);

            object obj = _pacManMap.GameMap[nextPosition.X, nextPosition.Y];

            if (obj is PMan pMan)
            {
                ReleaseInnerObject();
                SetCurrentPosition(nextPosition.X, nextPosition.Y);
                return Accept(pMan);
            }
            else if (obj is Ghost ghost)
            {
                ReleaseInnerObject();
                ghost._innerList.Add(this);
                _isInsideInnerList = true;
                SetCurrentPosition(nextPosition.X, nextPosition.Y);
            }
            else
            {
                IVisited visited = (IVisited)obj;
                if (visited.IsWalkable || visited is Gate)
                {
                    ReleaseInnerObject();
                    _innerList.Add(obj);
                    SetCurrentPosition(nextPosition.X, nextPosition.Y);
                    _pacManMap.GameMap[nextPosition.X, nextPosition.Y] = this;
                }
            }
            return false;
        }

        public void ReleaseInnerObject()
        {
            var tmp = _innerList.Where((x) => x is Ghost).FirstOrDefault();

            if(tmp != null)
            {
                // Object in innerList is ghost.
                _innerList.Remove(tmp);
                // Check for other objects in the list.
                if (_innerList.Count > 0)
                {
                    // Add this objects to innerList tmp(ghost).
                    foreach(var o in _innerList)
                    {
                        ((Ghost)tmp)._innerList.Add(o);
                    }
                }
                ((Ghost)tmp)._isInsideInnerList = false;
                _pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = tmp;
            }
            else if(_innerList.Count > 0)
            {
                // Some eatable or empty.
                _pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = _innerList[0];
            }
            else if(_isInsideInnerList && !(_pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] is PMan))
            {
                ((Ghost)_pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y])._innerList.Remove(this);
                _isInsideInnerList = false;
            }
            else
            {
                _pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = new Empty(new Position(CurrentPosition.X, CurrentPosition.Y));
            }

            _innerList.Clear();
        }

        public bool Eat(FoodContext foodContext)
        {
            if(!IsDead)
            {
                foodContext.AddScore(Points);
            }

            // Check for eatables in innerList.
            foreach (var o in _innerList)
            {
                if(o is Ghost ghost)
                {
                    ghost._isInsideInnerList = false;
                    if (ghost.IsFrightened)
                    {
                        ghost.Eat(foodContext);
                    }
                    else
                    {
                        foodContext.UseLife();
                        return true;
                    }
                }
                else if (o is IEatable eatable)
                {
                    eatable.Eat(foodContext);
                }
            }
            _innerList.Clear();
            if(!IsDead)
            {
                IsDead = true;
                GoToSpawn();
            }
            return false;
        }

        public void Spawn(int X, int Y)
        {
            // mb no need, because i'm making release before spawn
            if(_innerList.Count > 0)
            {
                var tmp = _innerList.Where(x => (x is IEatable && !(x is Ghost))).FirstOrDefault();
                if(tmp != null)
                {
                    _pacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = tmp;
                }
            }
            SetCurrentPosition(X, Y);
            EndPosition = CurrentPosition;
            // Check for other ghosts at current position. 
            if (_pacManMap.GameMap[X, Y] is Ghost && _pacManMap.GameMap[X, Y] != this)
            {
                ((Ghost)_pacManMap.GameMap[X, Y])._innerList.Add(this);
            }
            else
                _pacManMap.GameMap[X, Y] = this;
            SetDefaultBehavior();
            IsDead = false;
            _isNeedToRespawn = false;
        }

        public void SetDefaultBehavior()
        {
            Behavior = DefaultBehavior;
            IsFrightened = false;
        }

        public void SetFrighenedBehavior()
        {
            Behavior = new FrightenedBehavior(_pacManMap, TpPoint);
            IsFrightened = true;
        }

        public bool SetPath(Position position)
        {
            if(_isNeedToRespawn)
            {
                // Change behavior 'dead -> default'.
                _isNeedToRespawn = false;
                IsDead = false;
                IsFrightened = false;
                Behavior = DefaultBehavior;
            }
            // Set path.
            _path = Behavior.Execute(CurrentPosition, position);

            if (_path.Count > 0)
            {
                Position goToPosition = new Position(_path[_path.Count - 1].X, _path[_path.Count - 1].Y);
                EndPosition = goToPosition;
            }

            if (position.X != DefaltPosition.X && position.Y != DefaltPosition.Y)
            {
                return Move();
            }
            else
                return false;
        }

        public bool GetInto(FoodContext foodContext)
        {
            if (IsFrightened)
            {
                return Eat(foodContext);
            }
            else
            {
                foodContext.UseLife();
                return true;
            }
        }
    }

    public class PositionEventArgs : EventArgs
    {
        public Position NextPosition { get; private set; }

        public PositionEventArgs(int X, int Y)
        {
            NextPosition = new Position(X, Y);
        }
    }
}