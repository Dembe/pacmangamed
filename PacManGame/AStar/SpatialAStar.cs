﻿using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;

namespace PacManGame.AStar
{
    public interface IPathNode<in TUserContext>
    {
        Boolean IsWalkable(TUserContext inContext);
    }

    public interface IIndexedObject
    {
        int Index { get; set; }
    }

    public class MyPathNode : IPathNode<Object>
    {
        public Int32 X { get; set; }
        public Int32 Y { get; set; }
        public Boolean IsWall { get; set; }

        public bool IsWalkable(Object inContext)
        {
            return !IsWall;
        }
    }

    public class SpatialAStar<TPathNode, TUserContext> where TPathNode : IPathNode<TUserContext>
    {
        private readonly OpenCloseMap m_ClosedSet;
        private readonly OpenCloseMap m_OpenSet;
        private readonly PriorityQueue<PathNode> m_OrderedOpenSet;
        private readonly PathNode[,] m_CameFrom;
        private readonly OpenCloseMap m_RuntimeGrid;
        private readonly PathNode[,] m_SearchSpace;
        private readonly TeleportationPoint m_TeleportationPoint;

        public TPathNode[,] SearchSpace { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        protected class PathNode : IPathNode<TUserContext>, IComparer<PathNode>, IIndexedObject
        {
            public static readonly PathNode Comparer = new PathNode(0, 0, default(TPathNode));

            public TPathNode UserContext { get; internal set; }
            public Double G { get; internal set; }
            public Double H { get; internal set; }
            public Double F { get; internal set; }
            public int Index { get; set; }

            public Boolean IsWalkable(TUserContext inContext)
            {
                return UserContext.IsWalkable(inContext);
            }

            public int X { get; internal set; }
            public int Y { get; internal set; }

            public int Compare(PathNode x, PathNode y)
            {
                if (x.F < y.F)
                    return -1;
                else if (x.F > y.F)
                    return 1;

                return 0;
            }

            public PathNode(int inX, int inY, TPathNode inUserContext)
            {
                X = inX;
                Y = inY;
                UserContext = inUserContext;
            }
        }

        public SpatialAStar(TPathNode[,] inGrid, TeleportationPoint tpPoint)
        {
            SearchSpace = inGrid;
            Width = inGrid.GetLength(0);
            Height = inGrid.GetLength(1);
            m_SearchSpace = new PathNode[Width, Height];
            m_ClosedSet = new OpenCloseMap(Width, Height);
            m_OpenSet = new OpenCloseMap(Width, Height);
            m_CameFrom = new PathNode[Width, Height];
            m_RuntimeGrid = new OpenCloseMap(Width, Height);
            m_OrderedOpenSet = new PriorityQueue<PathNode>(PathNode.Comparer);
            m_TeleportationPoint= tpPoint;

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (inGrid[x, y] == null)
                        throw new ArgumentNullException();

                    m_SearchSpace[x, y] = new PathNode(x, y, inGrid[x, y]);
                }
            }
        }

        protected virtual Double Heuristic(PathNode inStart, PathNode inEnd)
        {
            if ((inStart.X == m_TeleportationPoint.FirstPoint.X && inStart.Y == m_TeleportationPoint.FirstPoint.Y &&
                inEnd.X == m_TeleportationPoint.SecondPoint.X && inEnd.Y == m_TeleportationPoint.SecondPoint.Y) || 
                (inStart.X == m_TeleportationPoint.SecondPoint.X && inStart.Y == m_TeleportationPoint.SecondPoint.Y &&
                inEnd.X == m_TeleportationPoint.FirstPoint.X && inEnd.Y == m_TeleportationPoint.FirstPoint.Y))
                return 1;
            return Math.Sqrt((inStart.X - inEnd.X) * (inStart.X - inEnd.X) + (inStart.Y - inEnd.Y) * (inStart.Y - inEnd.Y));
        }

        protected virtual Double NeighborDistance(PathNode inStart, PathNode inEnd)
        {
            int diffX = Math.Abs(inStart.X - inEnd.X);
            int diffY = Math.Abs(inStart.Y - inEnd.Y);
            int diff = diffX + diffY;
            if (diff == 1)
                return 1;
            else if (diff == 0)
                return 0;
            else if (diff == (m_TeleportationPoint.SecondPoint.X - m_TeleportationPoint.FirstPoint.X))
                return 1;
            else return 0;
        }

        public List<MyPathNode> Search(Position inStartNode, Position inEndNode, TUserContext inUserContext)
        {
            PathNode startNode = m_SearchSpace[inStartNode.X, inStartNode.Y];
            PathNode endNode = m_SearchSpace[inEndNode.X, inEndNode.Y];

            if (startNode == endNode)
                return new List<MyPathNode>();

            PathNode[] neighborNodes = new PathNode[4];

            m_ClosedSet.Clear();
            m_OpenSet.Clear();
            m_RuntimeGrid.Clear();
            m_OrderedOpenSet.Clear();

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    m_CameFrom[x, y] = null;
                }
            }

            startNode.G = 0;
            startNode.H = Heuristic(startNode, endNode);
            startNode.F = startNode.H;

            m_OpenSet.Add(startNode);
            m_OrderedOpenSet.Push(startNode);

            m_RuntimeGrid.Add(startNode);

            int nodes = 0;


            while (!m_OpenSet.IsEmpty)
            {
                PathNode x = m_OrderedOpenSet.Pop();

                if (x == endNode)
                {
                    List<MyPathNode> result = ReconstructPath(m_CameFrom, m_CameFrom[endNode.X, endNode.Y]);

                    result.Add(new MyPathNode { X = endNode.X, Y = endNode.Y });

                    return result;
                }

                m_OpenSet.Remove(x);
                m_ClosedSet.Add(x);

                StoreNeighborNodes(x, neighborNodes);

                for (int i = 0; i < neighborNodes.Length; i++)
                {
                    PathNode y = neighborNodes[i];
                    Boolean tentative_is_better;

                    if (y == null)
                        continue;

                    if (!y.UserContext.IsWalkable(inUserContext))
                        continue;

                    if (m_ClosedSet.Contains(y))
                        continue;

                    nodes++;

                    Double tentative_g_score = m_RuntimeGrid[x].G + NeighborDistance(x, y);
                    Boolean wasAdded = false;

                    if (!m_OpenSet.Contains(y))
                    {
                        m_OpenSet.Add(y);
                        tentative_is_better = true;
                        wasAdded = true;
                    }
                    else if (tentative_g_score < m_RuntimeGrid[y].G)
                    {
                        tentative_is_better = true;
                    }
                    else
                    {
                        tentative_is_better = false;
                    }

                    if (tentative_is_better)
                    {
                        m_CameFrom[y.X, y.Y] = x;

                        if (!m_RuntimeGrid.Contains(y))
                            m_RuntimeGrid.Add(y);

                        m_RuntimeGrid[y].G = tentative_g_score;
                        m_RuntimeGrid[y].H = Heuristic(y, x);
                        m_RuntimeGrid[y].F = m_RuntimeGrid[y].G + m_RuntimeGrid[y].H;

                        if (wasAdded)
                            m_OrderedOpenSet.Push(y);
                        else
                            m_OrderedOpenSet.Update(y);
                    }
                }
            }

            return new List<MyPathNode>();
        }

        private List<MyPathNode> ReconstructPath(PathNode[,] came_from, PathNode current_node)
        {
            List<MyPathNode> result = new List<MyPathNode>();

            ReconstructPathRecursive(came_from, current_node, result);

            return result;
        }

        private void ReconstructPathRecursive(PathNode[,] came_from, PathNode current_node, List<MyPathNode> result)
        {
            PathNode item = came_from[current_node.X, current_node.Y];

            if (item != null)
            {
                ReconstructPathRecursive(came_from, item, result);
                result.Add(new MyPathNode { X = current_node.X, Y = current_node.Y });
            }
        }

        private void StoreNeighborNodes(PathNode inAround, PathNode[] inNeighbors)
        {
            int x = inAround.X;
            int y = inAround.Y;

            if (y > 0)
                inNeighbors[0] = m_SearchSpace[x, y - 1];
            else
                inNeighbors[0] = null;

            if (x > 0)
                inNeighbors[1] = m_SearchSpace[x - 1, y];
            else
                inNeighbors[1] = null;

            if (x < Width - 1)
                inNeighbors[2] = m_SearchSpace[x + 1, y];
            else
                inNeighbors[2] = null;

            if (y < Height - 1)
                inNeighbors[3] = m_SearchSpace[x, y + 1];
            else
                inNeighbors[3] = null;

            if (x == m_TeleportationPoint.FirstPoint.X && y == m_TeleportationPoint.FirstPoint.Y)
                inNeighbors[1] = m_SearchSpace[m_TeleportationPoint.SecondPoint.X, m_TeleportationPoint.SecondPoint.Y];
            else if (x == m_TeleportationPoint.SecondPoint.X && y == m_TeleportationPoint.SecondPoint.Y)
                inNeighbors[2] = m_SearchSpace[m_TeleportationPoint.FirstPoint.X, m_TeleportationPoint.FirstPoint.Y];
        }

        private class OpenCloseMap
        {
            private readonly PathNode[,] m_Map;
            public int Width { get; private set; }
            public int Height { get; private set; }
            public int Count { get; private set; }

            public PathNode this[PathNode Node]
            {
                get
                {
                    return m_Map[Node.X, Node.Y];
                }

            }

            public bool IsEmpty
            {
                get
                {
                    return Count == 0;
                }
            }

            public OpenCloseMap(int inWidth, int inHeight)
            {
                m_Map = new PathNode[inWidth, inHeight];
                Width = inWidth;
                Height = inHeight;
            }

            public void Add(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (item != null)
                    throw new ApplicationException();
#endif

                Count++;
                m_Map[inValue.X, inValue.Y] = inValue;
            }

            public bool Contains(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

                if (item == null)
                    return false;

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                return true;
            }

            public void Remove(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                Count--;
                m_Map[inValue.X, inValue.Y] = null;
            }

            public void Clear()
            {
                Count = 0;

                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        m_Map[x, y] = null;
                    }
                }
            }
        }
    }
}
