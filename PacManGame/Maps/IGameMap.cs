﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.Maps
{
    public interface IGameMap
    {
        /// <summary>
        /// Initiation all game fields.
        /// </summary>
        /// <param name="gameEngine"></param>
        void GameInit(GameEngine gameEngine);

        int Level { get; }
    }
}
