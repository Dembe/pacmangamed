﻿using PacManGame.AStar;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.GhostBehavior
{
    class StupidBehavior : IBehavior
    {
        private readonly TeleportationPoint _tpPoint;

        public Map PacManMap { get; private set; }

        public StupidBehavior(Map map, TeleportationPoint teleportationPoint)
        {
            PacManMap = map;
            _tpPoint = teleportationPoint;
        }

        public List<MyPathNode> Execute(Position start, Position end)
        {
            List<MyPathNode> path;

            var converter = new MyPathNodeConverter();
            MyPathNode[,] grid = converter.Convert(PacManMap.GameMap);

            double distance = Math.Sqrt((start.X - end.X) * (start.X - end.X) + (start.Y - end.Y) * (start.Y - end.Y));
            var diffusionPoint = new Position(1, PacManMap.GameMap.GetLength(1) - 1);

            SpatialAStar<MyPathNode, Object> spatialAStar = new SpatialAStar<MyPathNode, object>(grid, _tpPoint);

            if ((distance <= 8) && !((start.X == diffusionPoint.X) && (start.Y == diffusionPoint.Y)))
            {
                Position goTo = Randomizer.GetRandPosition(PacManMap, diffusionPoint, 1);
                path = spatialAStar.Search(start, goTo, null);
            }
            else
            {
                path = spatialAStar.Search(start, end, null);
            }

            return path;
        }
    }
}
