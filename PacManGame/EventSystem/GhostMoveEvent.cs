﻿using PacManGame.EatableModels;
using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class GhostMoveEvent : Event
    {
        public Position PrevPosition { get; private set; }

        public Ghost CurrentGhost { get; private set; }

        public GameEngine CurrentGameEngine { get; private set; }

        public GhostMoveEvent(Position position, Ghost ghost, GameEngine gameEngine)
        {
            PrevPosition = position;
            CurrentGhost = ghost;
            CurrentGameEngine = gameEngine;
        }

    }
}
