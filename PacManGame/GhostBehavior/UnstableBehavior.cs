﻿using PacManGame.AStar;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.GhostBehavior
{
    public class UnstableBehavior : IBehavior
    {
        private readonly TeleportationPoint _tpPoint;

        public Map PacManMap { get; private set; }

        public UnstableBehavior(Map map, TeleportationPoint teleportationPoint)
        {
            PacManMap = map;
            _tpPoint = teleportationPoint;
        }

        public List<MyPathNode> Execute(Position start, Position end)
        {
            var converter = new MyPathNodeConverter();
            MyPathNode[,] grid = converter.Convert(PacManMap.GameMap);

            Position goTo = Randomizer.GetRandPosition(PacManMap, end, 7);

            SpatialAStar<MyPathNode, Object> spatialAStar = new SpatialAStar<MyPathNode, object>(grid, _tpPoint);
            List<MyPathNode> path = spatialAStar.Search(start, goTo, null);

            return path;
        }
    }
}
