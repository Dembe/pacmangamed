﻿using PacManGame.MainModels;
using System;

namespace PacManGame.EatableModels
{
    public class Energizer : IEatable
    {
        public Position CurrentPosition { get; private set; }

        public bool IsWalkable { get; private set; }

        public int Points { get; private set; }

        public Energizer(Position position)
        {
            CurrentPosition = position;
            IsWalkable = true;
            Points = 150;
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public bool Eat(FoodContext foodContext)
        {
            foodContext.AddScore(Points);
            foodContext.SetAllGhostsFrightened();
            return true;
        }

        public bool GetInto(FoodContext foodContext)
        {
            Eat(foodContext);
            return false;
        }
    }
}
