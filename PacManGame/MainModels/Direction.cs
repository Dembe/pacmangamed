﻿namespace PacManGame.MainModels
{
    public class Direction
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Direction()
        {
            SetLeftDirection();
        }

        public Direction(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void SetLeftDirection()
        {
            X = -1;
            Y = 0;
        }

        public void SetRightDirection()
        {
            X = 1;
            Y = 0;
        }

        public void SetUpDirection()
        {
            X = 0;
            Y = -1;
        }

        public void SetDownDirection()
        {
            X = 0;
            Y = 1;
        }
    }

}