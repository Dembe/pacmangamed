﻿using Microsoft.AspNetCore.SignalR;
using PacManGame.EventSystem;
using PacManGame.MainModels;
using PacManGame.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PacManGame.WEB
{
    public class GameEngineList
    {
        // Saves HubContext.ConnectionID
        public Dictionary<string, GameEngine> MainGameList = new Dictionary<string, GameEngine>();

        // Saves HttpContext.Connection.ID
        public Dictionary<string, GameEngine> SubGameList = new Dictionary<string, GameEngine>();

        public GameEngine Tmp { get; set; }

        private readonly IHubContext<GameHub> _hubContext;

        public GameEngineList(IHubContext<GameHub> hubContext)
        {
            _hubContext = hubContext;
        }

        private void SubscribeGameEngine(GameEngine gameEngine)
        {
            gameEngine.GameEventSink.Subscribe<PacManMoveEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<GhostMoveEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<RespawnEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<GhostAddedEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<BerryAddedEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<GameOverEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<NextLevelEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Subscribe<RemoveObjectEvent>(new NotificationManager(_hubContext, this).Handle);
        }

        private void UnSubscribeGameEngine(GameEngine gameEngine)
        {
            gameEngine.GameEventSink.Unsubscribe<PacManMoveEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<GhostMoveEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<RespawnEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<GhostAddedEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<BerryAddedEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<GameOverEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<NextLevelEvent>(new NotificationManager(_hubContext, this).Handle);
            gameEngine.GameEventSink.Unsubscribe<RemoveObjectEvent>(new NotificationManager(_hubContext, this).Handle);
        }

        public void AddNewMainGameToDictionary(string hubConnID, string httpConnID)
        {
            RemoveGameFromMainDictionary(hubConnID);

            string key = null;

            foreach (var sGame in SubGameList)
            {
                if(sGame.Key == httpConnID)
                {
                    MainGameList.Add(hubConnID, sGame.Value);
                    key = sGame.Key;
                }
            }
            if(key != null)
                SubGameList.Remove(key);
        }

        public void AddNewSubGameToDictionary(string httpConnID, GameEngine gameEngine)
        {
            RemoveGameFromSubDictionary(httpConnID);

            SubscribeGameEngine(gameEngine);

            SubGameList.Add(httpConnID, gameEngine);
        }

        public void RemoveGameFromMainDictionary(string hubConnID)
        {
            string key = null;
            foreach(var mGame in MainGameList)
            {
                if (mGame.Key == hubConnID)
                {
                    UnSubscribeGameEngine(mGame.Value);
                    key = mGame.Key;
                }
            }

            if (key != null)
                MainGameList.Remove(key);
        }

        private void RemoveGameFromSubDictionary(string httpConnID)
        {
            string key = null;
            foreach (var sGame in SubGameList)
            {
                if (sGame.Key == httpConnID)
                {
                    key = sGame.Key;
                }
            }

            if (key != null)
                SubGameList.Remove(key);
        }


        public bool Find(string connID)
        {
            foreach(var d in MainGameList)
            {
                if (d.Key == connID)
                    return true;
            }
            return false;
        }

        public string GetConnID(GameEngine gameEngine)
        {
            foreach(var d in MainGameList)
            {
                if (d.Value == gameEngine)
                    return d.Key;
            }
            return null;
        }

        public GameEngine GetGameEngine(string connID)
        {
            foreach (var d in MainGameList)
            {
                if (d.Key == connID)
                    return d.Value;
            }
            return null;
        }
    }
}
