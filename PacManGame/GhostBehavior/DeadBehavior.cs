﻿using PacManGame.AStar;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.GhostBehavior
{
    // Dead behavior = pursuit behavior to default (spawn) position.
    public class DeadBehavior : IBehavior
    {
        private readonly TeleportationPoint _tpPoint;

        public Map PacManMap { get; private set; }

        public DeadBehavior(Map map, TeleportationPoint teleportationPoint)
        {
            PacManMap = map;
            _tpPoint = teleportationPoint;
        }

        public List<MyPathNode> Execute(Position start, Position end)
        {
            var converter = new MyPathNodeConverter();
            MyPathNode[,] grid = converter.Convert(PacManMap.GameMap);

            SpatialAStar<MyPathNode, Object> spatialAStar = new SpatialAStar<MyPathNode, object>(grid, _tpPoint);
            List<MyPathNode> path = spatialAStar.Search(start, end, null);

            return path;
        }
    }
}
