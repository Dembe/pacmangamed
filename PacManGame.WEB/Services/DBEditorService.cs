﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using PacManGame.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PacManGame.WEB
{
    public class DBEditorService
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;

        public DBEditorService(UserManager<User> userManager, IHttpContextAccessor contextAccessor)
        {
            _userManager = userManager;
            _contextAccessor = contextAccessor;
        }

        public async Task SetUserTopScore(int score)
        {
            ClaimsPrincipal currentUser = _contextAccessor.HttpContext.User;
            if (_userManager == null)
                throw new Exception();
            var user = await _userManager.GetUserAsync(currentUser);
            if(user != null)
            {
                if(user.TopScore < score)
                {
                    user.TopScore = score;
                    await _userManager.UpdateAsync(user);
                }
            }
        }

    }
}
