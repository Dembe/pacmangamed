﻿using System;

namespace PacManGame.DAL.Entities
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public DateTime Time { get; set; }
    }
}
