﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using PacManGame.EatableModels;
using PacManGame.GhostBehavior;
using PacManGame.MainModels;

namespace PacManGame.MapBuilder.XMLBuilder
{
    public class XMLMapBuilder : IMapBuilder
    {
        private Map _XMLMAP;
        private readonly GameEngine _gameEngine;

        private const string _imgRed = "/ghost-red.png";
        private const string _imgPink = "/ghost-pink.png";
        private const string _imgBlue = "/ghost-blue.png";
        private const string _imgOrange = "/ghost-orange.png";

        public XMLMapBuilder(GameEngine gameEngine)
        {
            _gameEngine = gameEngine;
        }

        public object[,] BuildMap(FoodContext foodContext)
        {

            object[,] system_map = new object[_XMLMAP.Grid.Width, _XMLMAP.Grid.Height];

            // Add TP point.
            TeleportationPoint teleportationPoint = new TeleportationPoint(new Position(0, 0), new Position(0, 0));
            _gameEngine.GameTPPoint = teleportationPoint;

            // Create all static cells.
            foreach (var gridCell in _XMLMAP.Grid.Cell.Where(cell => cell != null))
            {
                var cell = CreateCell(gridCell, foodContext, teleportationPoint);
                if(cell != null)
                    system_map[cell.CurrentPosition.X, cell.CurrentPosition.Y] = cell;
            }

            // Add PacMan.
            var pMan = CreatePacMan(_XMLMAP.Pacman);
            pMan.TpPoint = teleportationPoint;
            pMan.GameFoodContext = _gameEngine.GameFoodContext;
            _gameEngine.PacMan = pMan;
            system_map[pMan.CurrentPosition.X, pMan.CurrentPosition.Y] = pMan;

            // Add Ghosts.
            _XMLMAP.Ghosts.Ghost.ForEach(sprite => {
                CreateGhost(sprite, teleportationPoint);
            });

            return system_map;
        }

        public void LoadSingle(string filename)
        {
            _XMLMAP = Deserialize<Map>(File.ReadAllText(filename));
        }

        private IObject CreateCell(object parameter, FoodContext foodContext, TeleportationPoint teleportationPoint)
        {
            if (parameter is Cell cell)
            {
                int column = cell.Column - 1;
                int row = cell.Row - 1;
                Position position = new Position(column, row);

                var sprite = cell.GetAtomicValues()
                    .Where(x => x != null)
                    .Select(x => CreateSprite(position, x, foodContext, teleportationPoint))
                    .SingleOrDefault(x => x != null);

                return sprite;
            }

            return null;
        }

        private IObject CreateSprite(Position position, object sprite, FoodContext foodContext, TeleportationPoint teleportationPoint)
        {
            switch (sprite)
            {
                case WallCell b:
                    return new Wall(position);
                case GateCell g:
                    return new Gate(position);
                case PointCell p when p.Class == PointType.Small:
                    foodContext.CountOfPoints++;
                    return new PointFood(position);
                case PointCell p when p.Class == PointType.Energizer:
                    return new Energizer(position);
                case EmptyCell e:
                    return new Empty(position);
                case TeleportationCell t:
                    if((teleportationPoint.FirstPoint.X == 0 && teleportationPoint.FirstPoint.Y == 0))
                    {
                        teleportationPoint.FirstPoint = position;
                    }
                    else
                    {
                        teleportationPoint.SecondPoint = position;
                    }
                    return new Empty(position);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private PMan CreatePacMan(PacManCell sprite)
        {
            return new PMan(_gameEngine.GameMap, (sprite.Column - 1), (sprite.Row - 1));
        }

        private void CreateGhost(GhostCell sprite, TeleportationPoint teleportationPoint)
        {
            Ghost ghost;
            switch (sprite)
            {
                case GhostCell g when g.Class == GhostBehaviorType.Shadow:
                    ghost = new Ghost(new PursuitBehavior(_gameEngine.GameMap, teleportationPoint), _gameEngine.GameMap, sprite.Column - 1, sprite.Row - 1, _imgRed, true);
                    break;
                case GhostCell g when g.Class == GhostBehaviorType.Speedy:
                    ghost = new Ghost(new AmbusherBehavior(_gameEngine.GameMap, teleportationPoint), _gameEngine.GameMap, sprite.Column - 1, sprite.Row - 1, _imgPink, true);
                    break;
                case GhostCell g when g.Class == GhostBehaviorType.Bashful:
                    ghost = new Ghost(new UnstableBehavior(_gameEngine.GameMap, teleportationPoint), _gameEngine.GameMap, sprite.Column - 1, sprite.Row - 1, _imgBlue, false);
                    break;
                case GhostCell g when g.Class == GhostBehaviorType.Pokey:
                    ghost = new Ghost(new StupidBehavior(_gameEngine.GameMap, teleportationPoint), _gameEngine.GameMap, sprite.Column - 1, sprite.Row - 1, _imgOrange, false);
                    break;
                default:
                    ghost = null;
                    break;
            }
            _gameEngine.GameFoodContext.Ghosts.Add(ghost);
            ghost.TpPoint = teleportationPoint;
        }

        public static T Deserialize<T>(string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }

        public static string Serialize<T>(T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringWriter textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }

        [XmlRoot(ElementName = "brick")]
        public class WallCell
        {
        }

        [XmlRoot(ElementName = "gate")]
        public class GateCell
        {
        }

        [XmlRoot(ElementName = "empty")]
        public class EmptyCell
        {
        }

        [XmlRoot(ElementName = "teleportation")]
        public class TeleportationCell
        {
        }

        public enum PointType
        {
            [XmlEnum("small")]
            Small,
            [XmlEnum("energizer")]
            Energizer
        }

        [XmlRoot(ElementName = "point")]
        public class PointCell
        {
            [XmlAttribute(AttributeName = "class")]
            public PointType Class { get; set; }
        }

        [XmlRoot(ElementName = "ghosts")]
        public class Ghosts
        {
            [XmlElement(ElementName = "ghost")]
            public List<GhostCell> Ghost { get; set; }
        }

        public enum GhostBehaviorType
        {
            [XmlEnum("shadow")]
            Shadow,
            [XmlEnum("speedy")]
            Speedy,
            [XmlEnum("bashful")]
            Bashful,
            [XmlEnum("pokey")]
            Pokey
        }

        [XmlRoot(ElementName = "ghost")]
        public class GhostCell
        {
            [XmlAttribute(AttributeName = "class")]
            public GhostBehaviorType Class { get; set; }

            [XmlAttribute(AttributeName = "row")]
            public int Row { get; set; }

            [XmlAttribute(AttributeName = "column")]
            public int Column { get; set; }
        }

        [XmlRoot(ElementName = "pacman")]
        public class PacManCell
        {
            [XmlAttribute(AttributeName = "row")]
            public int Row { get; set; }

            [XmlAttribute(AttributeName = "column")]
            public int Column { get; set; }
        }

        [XmlRoot(ElementName ="wallColor")]
        public class WallColor
        {
            [XmlAttribute(AttributeName = "color")]
            public string Color { get; set; }
        }

        [XmlRoot(ElementName = "energizerColor")]
        public class EnergizerColor
        {
            [XmlAttribute(AttributeName = "color")]
            public string Color { get; set; }
        }

        [XmlRoot(ElementName = "grid")]
        public class Grid
        {
            [XmlElement(ElementName = "cell")]
            public List<Cell> Cell { get; set; }

            [XmlAttribute(AttributeName = "height")]
            public int Height { get; set; }

            [XmlAttribute(AttributeName = "width")]
            public int Width { get; set; }
        }

        [XmlRoot(ElementName = "cell")]
        public class Cell
        {
            [XmlAttribute(AttributeName = "row")]
            public int Row { get; set; }

            [XmlAttribute(AttributeName = "column")]
            public int Column { get; set; }

            [XmlElement(ElementName = "brick")]
            public WallCell Wall { get; set; }

            [XmlElement(ElementName = "gate")]
            public GateCell Gate { get; set; }

            [XmlElement(ElementName = "empty")]
            public EmptyCell Empty { get; set; }

            [XmlElement(ElementName = "point")]
            public PointCell Point { get; set; }

            [XmlElement(ElementName = "teleportation")]
            public TeleportationCell Teleportation { get; set; }

            public object[] GetAtomicValues() => new object[]
                { Wall, Empty, Teleportation, Point, Gate };
        }

        [XmlRoot(ElementName = "map")]
        public class Map
        {
            [XmlElement(ElementName = "grid")]
            public Grid Grid { get; set; }

            [XmlElement(ElementName = "ghosts")]
            public Ghosts Ghosts { get; set; }

            [XmlElement(ElementName = "pacman")]
            public PacManCell Pacman { get; set; }

            [XmlElement(ElementName = "wallColor")]
            public WallColor WallColor { get; set; }

            [XmlElement(ElementName = "energizerColor")]
            public EnergizerColor EnergizerColor { get; set; }

            [XmlAttribute(AttributeName = "name")]
            public string Name { get; set; }
        }
    }
}
