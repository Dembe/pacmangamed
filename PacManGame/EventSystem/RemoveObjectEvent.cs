﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class RemoveObjectEvent : Event
    {
        public GameEngine CurrentGameEngine { get; private set; }

        public Position ObjectPosition { get; private set; }

        public RemoveObjectEvent(GameEngine gameEngine, Position position)
        {
            CurrentGameEngine = gameEngine;
            ObjectPosition = position;
        }

    }
}
