﻿namespace PacManGame.MainModels
{
    public class GameState
    {
        private int _score;
        private int _countOfLives;

        public GameState()
        {
            _score = 0;
            _countOfLives = 3;
        }

        public void AddScore(int score)
        {
            _score += score;
        }

        public void UseLife()
        {
            _countOfLives--;
        }

        public int GetScore()
        {
            return _score;
        }

        public int GetAmountOfLives()
        {
            return _countOfLives;
        }
    }

}
