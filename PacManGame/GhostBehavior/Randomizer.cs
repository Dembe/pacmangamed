﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.GhostBehavior
{
    public static class Randomizer
    {
        /// <summary>
        /// Returns a random position within a given radius.
        /// </summary>
        /// <param name="pacManMap"></param>
        /// <param name="goTo"></param>
        /// <param name="radius"></param>
        public static Position GetRandPosition(Map pacManMap, Position goTo, int radius)
        {
            Random rand = new Random();
            Position result = new Position(0, 0);
            int endRand = 4;

            for (int i = 0; i < 11; i++)
            {
                bool flag = false;
                int number = rand.Next(0, endRand);
                if (i == 10)
                {
                    if (radius == 1)
                    {
                        return new Position(0, 0);
                    }
                    else
                    {
                        radius--;
                        i = 0;
                    }
                }

                int pointY, pointX;

                switch (number)
                {
                    case 0:
                        pointY = goTo.Y + radius;
                        if (pointY >= pacManMap.GameMap.GetLength(1))
                        {
                            break;
                        }
                        if (!(pacManMap.GameMap[goTo.X, pointY] is Wall))
                        {
                            result = new Position(goTo.X, pointY);
                            flag = true;
                        }
                        break;
                    case 1:
                        pointY = goTo.Y - radius;
                        if (pointY <= 0)
                        {
                            break;
                        }
                        if (!(pacManMap.GameMap[goTo.X, pointY] is Wall))
                        {
                            result = new Position(goTo.X, pointY);
                            flag = true;
                        }
                        break;
                    case 2:
                        pointX = goTo.X + radius;
                        if (pointX >= pacManMap.GameMap.GetLength(0))
                        {
                            break;
                        }

                        if (!(pacManMap.GameMap[pointX, goTo.Y] is Wall))
                        {
                            result = new Position(pointX, goTo.Y);
                            flag = true;
                        }
                        break;
                    case 3:
                        pointX = goTo.X - radius;
                        if (pointX <= 0)
                        {
                            break;
                        }

                        if (!(pacManMap.GameMap[pointX, goTo.Y] is Wall))
                        {
                            result = new Position(pointX, goTo.Y);
                            flag = true;
                        }
                        break;
                }
                if (flag)
                    break;
            }
            return result;
        }
    }
}
