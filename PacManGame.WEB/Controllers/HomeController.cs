﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PacManGame.MainModels;
using PacManGame.Maps;
using PacManGame.WEB.Models;

namespace PacManGame.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly GameEngineList _pacManGameEngineList;

        public HomeController(GameEngineList gameEngineList, IHubContext<GameHub> gameHub)
        {
            _pacManGameEngineList = gameEngineList;
        }

        public IActionResult Index()
        {
            var gEngine = new GameEngine(new SecondMap());
            gEngine.GameInit();
            _pacManGameEngineList.AddNewSubGameToDictionary(HttpContext.Connection.Id, gEngine);

            return View("Index", gEngine.GameMap.GameMap);
        }

        public IActionResult FAQ()
        {
            return View();
        }

        public IActionResult NextMap(int gameMap, int score)
        {
            GameEngine gameEngine = null;
            switch (gameMap)
            {
                case 1:
                    gameEngine = new GameEngine(new ThirdMap());
                    break;
                case 2:
                    gameEngine = new GameEngine(new TestMap());
                    break;
                case 3:
                    gameEngine = new GameEngine(new MainMap());
                    break;
            }
            if(gameEngine != null)
            {
                gameEngine.GameInit();
                gameEngine.GameState.AddScore(score);
                _pacManGameEngineList.AddNewSubGameToDictionary(HttpContext.Connection.Id, gameEngine);
                return View("Index", gameEngine.GameMap.GameMap);
            }

            return View("Error");
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
