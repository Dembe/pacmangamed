﻿using PacManGame.MainModels;

namespace PacManGame.EatableModels
{
    public class PointFood : IEatable
    {
        public Position CurrentPosition { get; private set; }

        public bool IsWalkable { get; private set; }

        public int Points { get; private set; }

        public PointFood(Position position)
        {
            CurrentPosition = position;
            IsWalkable = true;
            Points = 10;
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public bool Eat(FoodContext foodContext)
        {
            foodContext.AddScore(Points);
            foodContext.PointEated();
            return true;
        }

        public bool GetInto(FoodContext foodContext)
        {
            Eat(foodContext);
            return false;
        }
    }
}