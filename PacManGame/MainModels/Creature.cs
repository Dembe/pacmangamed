﻿using System;

namespace PacManGame.MainModels
{
    public abstract class Creature : IObject
    {
        public Position CurrentPosition { get; protected set; }
        public Position DefaltPosition { get; protected set; }
        public int Speed { get; set; }
        public abstract bool Move();
    }
}
