﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.MainModels
{
    public class Gate : IObject, IVisited
    {
        public bool IsWalkable { get; private set; }
        public Position CurrentPosition { get; private set; }

        public Gate(Position position)
        {
            CurrentPosition = position;
            IsWalkable = false;
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public bool GetInto(FoodContext foodContext)
        {
            return false;
        }
    }
}
