﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacManGame.EventSystem;
using PacManGame.MainModels;

namespace PacManGame.Maps
{
    public class SecondMap : IGameMap
    {
        public int Level { get; private set; }

        public SecondMap()
        {
            Level = 1;
        }

        public void GameInit(GameEngine gameEngine)
        {
            gameEngine.GameState = new GameState();
            gameEngine.GameEventSink = new EventSink();
            gameEngine.GameFoodContext = new FoodContext(gameEngine.GameState);
            gameEngine.GameMap = new Map(gameEngine.GameFoodContext);
            
            //gameEngine.GameMap.ReadMapFromXML("D:/Visual Studio/Visual Studio C# Projects/PacManGame/PacManGame.WEB/wwwroot/maps/DefaultMap.cshtml", gameEngine);
            gameEngine.GameMap.ReadMapFromXML("h:/root/home/admindembe-001/www/site1/wwwroot/maps/DefaultMap.cshtml", gameEngine);

            gameEngine.IsBerryAdded = false;
            gameEngine.IsUnstableAdded = false;
            gameEngine.IsStupidAdded = false;

            gameEngine.PacMan.Eated += gameEngine.RespawnPacMan;

        }
    }
}
