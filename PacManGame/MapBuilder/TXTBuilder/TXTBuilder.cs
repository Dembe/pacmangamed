﻿using PacManGame.EatableModels;
using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PacManGame.MapBuilder
{
    public enum GridObjects
    {
        Empty,
        Wall,
        PointFood
    }

    public class TXTBuilder : IMapBuilder
    {
        private string[] _mapString { get; set; }

        private readonly List<GridObjectCreator> _gridObjectCreators = new List<GridObjectCreator>();

        public TXTBuilder(string path)
        {
            _gridObjectCreators.Add(new EmptyObject());
            _gridObjectCreators.Add(new WallObject());
            _gridObjectCreators.Add(new PointFoodObject());

            ReadFileToString(path);
        }

        private void ReadFileToString(string path)
        {
            if (path != null)
            {
                _mapString = File.ReadAllLines(path);
            }
        }

        public int GetSizeOfX()
        {
            if (_mapString == null)
                return 0;

            int sizeOfX = 0;
            foreach (var c in _mapString[0])
            {
                sizeOfX++;
            }
            return sizeOfX;
        }

        public int GetSizeOfY()
        {
            if (_mapString == null)
                return 0;
            return _mapString.GetLength(0);
        }

        public object[,] BuildMap(FoodContext foodContext)
        {
            int i = 0;
            int sizeOfX = GetSizeOfX();
            object[,] map = new object[GetSizeOfX(), GetSizeOfY()];
            foreach (var line in _mapString)
            {
                for (int j = 0; j < sizeOfX; j++)
                {
                    // Check enum number.
                    GridObjects gridObjects = (GridObjects)Convert.ToInt32(line[j].ToString());
                    // Factory build.
                    map[j, i] = _gridObjectCreators.First(x => x.GridObject == gridObjects).Create(j, i);
                    if (gridObjects == GridObjects.PointFood)
                    {
                        foodContext.CountOfPoints++;
                    }
                }
                i++;
            }
            return map;
        }
    }
}
