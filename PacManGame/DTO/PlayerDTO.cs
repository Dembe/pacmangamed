﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.DTO
{
    // Data Transfer Object (PacManGame.DAL)
    public class PlayerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public DateTime Time { get; set; }
    }
}
