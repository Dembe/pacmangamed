﻿using PacManGame.EatableModels;
using PacManGame.MapBuilder;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace PacManGame.MainModels
{
    public class PMan : Creature
    {
        private Directions _savedDirection = Directions.Empty;

        public Direction PacManDirection { get; private set; }
        public Map PacManMap { get; private set; }
        public FoodContext GameFoodContext { get; set; }
        public TeleportationPoint TpPoint { get; set; }

        public event EventHandler Eated;
        public event EventHandler<PositionEventArgs> PositionChanged;

        public PMan(Map newMap, int X, int Y)
        {
            Speed = 250;
            PacManDirection = new Direction();
            PacManDirection.SetLeftDirection();
            PacManMap = newMap;

            Position position = new Position(X, Y);
            CurrentPosition = position;
            DefaltPosition = position;
        }

        private void MakeAStep()
        {
            PacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = new Empty(new Position(CurrentPosition.X, CurrentPosition.Y));
            ChangePosition(CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y);
            PacManMap.GameMap[CurrentPosition.X, CurrentPosition.Y] = this;
        }

        private Position CheckDirection()
        {
            // Check for tp point.
            Position prevDirection = new Position(0, 0);
            if (CurrentPosition.Y + PacManDirection.Y == TpPoint.FirstPoint.Y && CurrentPosition.X + PacManDirection.X == TpPoint.FirstPoint.X - 1)
            {
                prevDirection.X = PacManDirection.X;
                prevDirection.Y = PacManDirection.Y;
                PacManDirection = new Direction(TpPoint.SecondPoint.X, PacManDirection.Y);
            }
            else if (CurrentPosition.Y + PacManDirection.Y == TpPoint.SecondPoint.Y && CurrentPosition.X + PacManDirection.X == TpPoint.SecondPoint.X + 1)
            {
                prevDirection.X = PacManDirection.X;
                prevDirection.Y = PacManDirection.Y;
                PacManDirection = new Direction(-TpPoint.SecondPoint.X, PacManDirection.Y);
            }
            return prevDirection;
        }

        private void CheckSaveDirection()
        {
            switch (_savedDirection)
            {
                case Directions.Empty:
                    break;
                case Directions.Up:
                    TurnUp();
                    break;
                case Directions.Left:
                    TurnLeft();
                    break;
                case Directions.Down:
                    TurnDown();
                    break;
                case Directions.Right:
                    TurnRight();
                    break;
            }
        }

        private bool CheckOnWall(int X, int Y)
        {
            if (PacManMap.GameMap[X, Y] is Wall)
                return true;
            return false;
        }

        private void ChangePosition(int X, int Y)
        {
            var positionEventArgs = new PositionEventArgs(CurrentPosition.X, CurrentPosition.Y);
            CurrentPosition = new Position(X, Y);
            PacManMap.GameMap[X, Y] = this;
            PositionChanged?.Invoke(this, positionEventArgs);
        }

        public bool Visit(IVisited visitedObject)
        {
            if (((IObject)visitedObject).CurrentPosition.X == CurrentPosition.X &&
                ((IObject)visitedObject).CurrentPosition.Y == CurrentPosition.Y &&
                visitedObject.GetInto(GameFoodContext))
            {
                // If 'GetInto' returns true it means PacMan dead.
                Eated?.Invoke(this, EventArgs.Empty);
                PacManDirection.SetLeftDirection();
                return true;
            }
            return false;
        }

        public override bool Move()
        {
            CheckSaveDirection();

            Position prevDirection = CheckDirection();

            IVisited visited = ((IVisited)PacManMap.GameMap[CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y]);
            if (visited.IsWalkable)
            {
                MakeAStep();
                visited.Accept(this);
            }
            if (!(prevDirection.X == 0 && prevDirection.Y == 0))
            {
                PacManDirection = new Direction(prevDirection.X, prevDirection.Y);
            }
            return true;
        }

        public void TurnLeft()
        {
            _savedDirection = Directions.Empty;

            Direction save = new Direction(PacManDirection.X, PacManDirection.Y);
            PacManDirection.SetLeftDirection();

            if (CheckOnWall(CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y))
            {
                PacManDirection = save;
                _savedDirection = Directions.Left;
            }
        }

        public void TurnRight()
        {
            _savedDirection = Directions.Empty;

            Direction save = new Direction(PacManDirection.X, PacManDirection.Y);
            PacManDirection.SetRightDirection();
            if (CheckOnWall(CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y))
            {
                PacManDirection = save;
                _savedDirection = Directions.Right;

            }
        }

        public void TurnUp()
        {
            _savedDirection = Directions.Empty;

            Direction save = new Direction(PacManDirection.X, PacManDirection.Y);
            PacManDirection.SetUpDirection();
            if (CheckOnWall(CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y))
            {
                PacManDirection = save;
                _savedDirection = Directions.Up;
            }
        }

        public void TurnDown()
        {
            _savedDirection = Directions.Empty;

            Direction save = new Direction(PacManDirection.X, PacManDirection.Y);
            PacManDirection.SetDownDirection();
            if(CheckOnWall(CurrentPosition.X + PacManDirection.X, CurrentPosition.Y + PacManDirection.Y))
            {
                PacManDirection = save;
                _savedDirection = Directions.Down;
            }
        }

        public void Spawn()
        {
            ChangePosition(DefaltPosition.X, DefaltPosition.Y);
        }

    }

    public enum Directions
    {
        Up,
        Right,
        Down,
        Left,
        Empty
    }
}
