﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class RespawnEvent : Event
    {
        public GameEngine CurrentGameEngine { get; private set; }

        public RespawnEvent(GameEngine gameEngine)
        {
            CurrentGameEngine = gameEngine;
        }
    }
}
