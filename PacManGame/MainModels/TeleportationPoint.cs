﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.MapBuilder
{
    public class TeleportationPoint
    {
        private readonly bool IsAdded = false;
        public Position FirstPoint { get; set; }
        public Position SecondPoint { get; set; }

        public TeleportationPoint(Position aPoint, Position bPoint)
        {
            IsAdded = true;

            FirstPoint = aPoint;
            SecondPoint = bPoint;
        }


    }
}
