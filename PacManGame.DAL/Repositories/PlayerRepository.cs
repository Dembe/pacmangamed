﻿using Microsoft.EntityFrameworkCore;
using PacManGame.DAL.EF;
using PacManGame.DAL.Entities;
using PacManGame.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PacManGame.DAL.Repositories
{
    public class PlayerRepository: IRepository<Player>
    {
        private readonly PlayerContext _db;

        public PlayerRepository(PlayerContext context)
        {
            _db = context;
        }

        public IEnumerable<Player> GetAll()
        {
            return _db.Players;
        }

        public Player Get(int id)
        {
            return _db.Players.Find(id);
        }

        public void Create(Player player)
        {
            _db.Players.Add(player);
        }

        public void Update(Player player)
        {
            _db.Entry(player).State = EntityState.Modified;
        }

        public IEnumerable<Player> Find(Func<Player, Boolean> predicate)
        {
            return _db.Players.Where(predicate).ToList();
        }

        public IEnumerable<Player> GetTop(int count)
        {
            return _db.Players.OrderByDescending((x) => x.Score).Take(count).ToList();
        }

        public void Delete(int id)
        {
            Player player = _db.Players.Find(id);
            if (player != null)
                _db.Players.Remove(player);
        }
    }
}
