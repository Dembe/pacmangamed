﻿using System;
using System.Collections.Generic;

namespace PacManGame.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        IEnumerable<T> GetTop(int count);
        void Delete(int id);
    }
}
