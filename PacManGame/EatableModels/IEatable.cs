﻿using PacManGame.MainModels;

namespace PacManGame.EatableModels
{
    public interface IEatable : IObject, IVisited
    {
        /// <summary>
        /// Object eated by PacMan.
        /// </summary>
        /// <param name="foodContext"></param>
        /// <returns>Returns true if object eated.</returns>
        bool Eat(FoodContext foodContext);

        int Points { get; }
    }
}
