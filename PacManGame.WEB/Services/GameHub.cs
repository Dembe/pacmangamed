﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using PacManGame.MainModels;
using PacManGame.WEB.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PacManGame.WEB
{
    public class GameHub : Hub
    {
        private readonly GameEngineList _pacManGameEngineList;
        private readonly DBEditorService _dBEditorService;

        public GameHub(GameEngineList gameEngineList, DBEditorService dBEditorService)
        {
            _pacManGameEngineList = gameEngineList;
            _dBEditorService = dBEditorService;
        }

        public async Task SetSprites()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if(gEngine != null)
            {
                int[,] GhostsPrevPosition = GetGhostsPrevPositionArray(gEngine);
                await this.Clients.Client(Context.ConnectionId).SendAsync("InitGhosts", gEngine.GameFoodContext.Ghosts.Count, GhostsPrevPosition);
                await this.Clients.Client(Context.ConnectionId).SendAsync("InitPacMan",gEngine.PacMan.CurrentPosition.X, gEngine.PacMan.CurrentPosition.Y, gEngine.GameState.GetAmountOfLives());


                for (int i = 0; i < gEngine.GameFoodContext.Ghosts.Count; i++)
                {
                    await this.Clients.Client(Context.ConnectionId).SendAsync("SetSRCGhost", i, gEngine.GameFoodContext.Ghosts[i].ImgUrl);
                }
            }
        }

        public void StartGame()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.GameStart();
            }
        }

        public void StopGame()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.GameStop();
            }
        }

        public async Task PrintScore()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                await this.Clients.Client(Context.ConnectionId).SendAsync("PrintScore", gEngine.GameState.GetScore());
            }
        }

        private int[,] GetGhostsPrevPositionArray(GameEngine gameEngine)
        {
            int[,] result = new int[gameEngine.GameFoodContext.Ghosts.Count, 2];
            for (int i = 0; i < gameEngine.GameFoodContext.Ghosts.Count; i++)
            {
                result[i, 0] = gameEngine.GameFoodContext.Ghosts[i].CurrentPosition.X;
                result[i, 1] = gameEngine.GameFoodContext.Ghosts[i].CurrentPosition.Y;
            }
            return result;
        }

        public override async Task OnConnectedAsync()
        {
            _pacManGameEngineList.AddNewMainGameToDictionary(Context.ConnectionId, Context.GetHttpContext().Connection.Id);

            await base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _pacManGameEngineList.RemoveGameFromMainDictionary(Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task SaveRecord()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                await _dBEditorService.SetUserTopScore(gEngine.GameState.GetScore());
                // OLD DB
                //gEngine.AddScoreToDB(nickname);
            }
        }

        public void TurnLeft()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.PacMan.TurnLeft();
            }
        }

        public void TurnUp()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.PacMan.TurnUp();
            }
        }

        public void TurnRight()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.PacMan.TurnRight();
            }
        }

        public void TurnDown()
        {
            var gEngine = _pacManGameEngineList.GetGameEngine(Context.ConnectionId);
            if (gEngine != null)
            {
                gEngine.PacMan.TurnDown();
            }
        }

    }
}
