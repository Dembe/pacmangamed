﻿using PacManGame.MainModels;

namespace PacManGame.MapBuilder
{
    public class WallObject : GridObjectCreator
    {
        public WallObject()
        {
            GridObject = GridObjects.Wall;
        }

        public override IObject Create(int X, int Y)
        {
            return new Wall(new Position(X, Y));
        }
    }
}
