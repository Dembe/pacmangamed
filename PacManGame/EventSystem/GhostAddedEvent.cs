﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class GhostAddedEvent: Event
    {
        public GameEngine CurrentGameEngine { get; private set; }

        public Position CurrentPosition { get; private set; }
        /// <summary>
        /// Adds new Ghost on map.
        /// </summary>
        /// <param name="gameEngine"></param>
        /// <param name="position"></param>
        public GhostAddedEvent(GameEngine gameEngine, Position position)
        {
            CurrentGameEngine = gameEngine;
            CurrentPosition = position;
        }
    }
}
