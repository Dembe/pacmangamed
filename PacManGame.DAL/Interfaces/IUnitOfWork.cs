﻿using PacManGame.DAL.Entities;
using System;

namespace PacManGame.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Player> Players { get; }
        void Save();
    }
}
