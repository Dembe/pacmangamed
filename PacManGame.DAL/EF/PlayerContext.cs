﻿using Microsoft.EntityFrameworkCore;
using PacManGame.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.DAL.EF
{
    public class PlayerContext: DbContext
    {
        public DbSet<Player> Players { get; set; }

        public PlayerContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-N0618Q8\\SQLEXPRESS;Database=PacManDB;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer("Data Source = SQL6005.site4now.net; Initial Catalog = DB_A3F30A_PManDB; User Id = DB_A3F30A_PManDB_admin; Password = 147369159qqq;");

        }
    }
}
