﻿using PacManGame.EatableModels;
using System.Collections.Generic;
using System.Timers;

namespace PacManGame.MainModels
{
    public class FoodContext
    {
        private GameState _gameState { get; set; }
        private readonly Timer _frightenedTimer;

        public int CountOfPoints { get; set; }
        public readonly List<Ghost> Ghosts = new List<Ghost>();

        public FoodContext(GameState gameS)
        {
            _gameState = gameS;
            CountOfPoints = 0;

            // Set frightened timer for 7 sec.
            _frightenedTimer = new Timer(7000);
            _frightenedTimer.Elapsed += new ElapsedEventHandler(SetAllGhostsDefaultBehavior);
            _frightenedTimer.AutoReset = false;
        }

        public void AddScore(int score)
        {
            _gameState.AddScore(score);
        }

        public void UseLife()
        {
            _gameState.UseLife();
        }

        public void PointEated()
        {
            CountOfPoints--;
        }

        public void SetAllGhostsFrightened()
        {
            foreach (var g in Ghosts)
            {
                g.SetFrighenedBehavior();
            }

            // Invoke frightened timer.
            _frightenedTimer.Start();
        }

        private void SetAllGhostsDefaultBehavior(object source, ElapsedEventArgs e)
        {
            foreach (var g in Ghosts)
            {
                if (g.IsDead)
                    continue;
                g.SetDefaultBehavior();
            }
        }

    }
}
