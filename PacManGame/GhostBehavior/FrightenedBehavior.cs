﻿using PacManGame.AStar;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;

namespace PacManGame.GhostBehavior
{
    class FrightenedBehavior : IBehavior
    {
        private readonly TeleportationPoint _tpPoint;

        public Map PacManMap { get; private set; }

        public FrightenedBehavior(Map map, TeleportationPoint teleportationPoint)
        {
            PacManMap = map;
            _tpPoint = teleportationPoint;
        }

        public List<MyPathNode> Execute(Position start, Position end)
        {
            // Convert object[,] to MyPathNode[,].
            var converter = new MyPathNodeConverter();
            MyPathNode[,] grid = converter.Convert(PacManMap.GameMap);

            // Get random position.
            end = Randomizer.GetRandPosition(PacManMap, start, 1);

            // Get shortest path.
            SpatialAStar<MyPathNode, Object> spatialAStar = new SpatialAStar<MyPathNode, object>(grid, _tpPoint);
            List<MyPathNode> path = spatialAStar.Search(start, end, null);

            return path;
        }
    }

}
