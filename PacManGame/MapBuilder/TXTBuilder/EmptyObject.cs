﻿using PacManGame.MainModels;

namespace PacManGame.MapBuilder
{
    public class EmptyObject : GridObjectCreator
    {
        public EmptyObject()
        {
            GridObject = GridObjects.Empty;
        }

        public override IObject Create(int X, int Y)
        {
            return new Empty(new Position(X, Y));
        }
    }
}
