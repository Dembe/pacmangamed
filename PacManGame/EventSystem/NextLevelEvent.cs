﻿using PacManGame.MainModels;
using PacManGame.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class NextLevelEvent : Event
    {
        public int GameLvl { get; private set; }
        public GameEngine CurrentGameEngine { get; private set; }
        public int Score { get; set; }

        public NextLevelEvent(int gameLvl, GameEngine gameEngine, int score)
        {
            GameLvl = gameLvl;
            CurrentGameEngine = gameEngine;
            Score = score;
        }
    }
}
