﻿using PacManGame.DAL.Entities;
using PacManGame.DAL.Repositories;
using PacManGame.DTO;
using PacManGame.EatableModels;
using PacManGame.EventSystem;
using PacManGame.GhostBehavior;
using PacManGame.MapBuilder;
using PacManGame.Maps;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;

namespace PacManGame.MainModels
{
    public class GameEngine
    {
        private readonly IGameMap _currentGameMap;
        private readonly Timer _gameTimer = new Timer();
        private readonly Timer _berryTimer = new Timer();
        private bool _isGameStarted = false;

        public PMan PacMan { get; set; }
        public Map GameMap { get; set; }
        public GameState GameState { get; set; }
        public FoodContext GameFoodContext { get; set; }
        public TeleportationPoint GameTPPoint { get; set; }
        public EventSink GameEventSink { get; set; }
        public MovementTimer GameMovementTimer { get; set; }

        public bool IsBerryAdded { get; set; }
        public bool IsUnstableAdded { get; set; }
        public bool IsStupidAdded { get; set; }

        public GameEngine(IGameMap gameMap)
        {
            _currentGameMap = gameMap;
        }

        public void GameInit()
        {
            _currentGameMap.GameInit(this);
            SubscribePositionChanged();
            ///TEST
            //InitAllTimers();

            _gameTimer.Elapsed += GameTimedEvent;
            _gameTimer.Interval = 400;
            //GameMovementTimer = new MovementTimer(PacMan, GameFoodContext.Ghosts);
        }

        private void InvokePositionChanged(object sender, PositionEventArgs e)
        {
            if (sender is PMan pMan)
            {
                GameEventSink.Publish(new PacManMoveEvent(e.NextPosition, this, pMan));
            }
            else if (sender is Ghost ghost)
            {
                GameEventSink.Publish(new GhostMoveEvent(e.NextPosition, ghost, this));
            }
        }

        private void GameTimedEvent(object source, ElapsedEventArgs e)
        {
            MovePacMan();
            MoveAllGhosts();
            CheckPoints();
        }

        private void MovePacMan()
        {
            PacMan.Move();
        }

        private void MoveAllGhosts()
        {
            bool IsNeedToStop = false;
            foreach (var g in GameFoodContext.Ghosts)
            {
                if (g.Active)
                {
                    if ((g.CurrentPosition.X == g.EndPosition.X) && (g.CurrentPosition.Y == g.EndPosition.Y))
                    {
                        IsNeedToStop = g.SetPath(PacMan.CurrentPosition);
                    }
                    else
                    {
                        IsNeedToStop = g.Move();
                    }
                    if (IsNeedToStop)
                        break;
                }
            }
        }

        private void SubscribePositionChanged()
        {
            foreach (var g in GameFoodContext.Ghosts)
            {
                g.PositionChanged += InvokePositionChanged;
            }
            PacMan.PositionChanged += InvokePositionChanged;
        }

        private void UnSubscribePositionChanged()
        {
            foreach (var g in GameFoodContext.Ghosts)
            {
                g.PositionChanged -= InvokePositionChanged;
            }
            PacMan.PositionChanged -= InvokePositionChanged;
        }

        private void NextLvl()
        {
            GameEventSink.Publish(new NextLevelEvent(_currentGameMap.Level, this, GameState.GetScore()));
        }

        private void SpawnAllGhosts()
        {
            foreach (var g in GameFoodContext.Ghosts)
            {
                g.ReleaseInnerObject();
                g.Spawn(g.DefaltPosition.X, g.DefaltPosition.Y);
            }
        }

        private void GameOver()
        {
            PacMan.Eated -= RespawnPacMan;
            GameStop();
            GameEventSink.Publish(new GameOverEvent(this));
        }

        private void RemoveBerry(object source, ElapsedEventArgs e)
        {
            GameMap.GameMap[PacMan.DefaltPosition.X, PacMan.DefaltPosition.Y] = new Empty(new Position(PacMan.DefaltPosition.X, PacMan.DefaltPosition.Y));
            GameEventSink.Publish(new RemoveObjectEvent(this, PacMan.DefaltPosition));
        }

        public void RespawnPacMan(object sender, EventArgs e)
        {
            _gameTimer.Stop();

            if (GameState.GetAmountOfLives() > 0)
            {
                GameMap.GameMap[PacMan.CurrentPosition.X, PacMan.CurrentPosition.Y] = new Empty(new Position(PacMan.CurrentPosition.X, PacMan.CurrentPosition.Y));
                SpawnAllGhosts();
                PacMan.Spawn();
            }
            else
            {
                GameOver();
            }
            GameEventSink.Publish(new RespawnEvent(this));
        }

        public void CheckPoints()
        {
            int count = GameFoodContext.CountOfPoints;
            if (count <= 0)
                NextLvl();
            else if (count <= 100 && !IsBerryAdded)
            {
                Berry berry = new Berry(new Position(PacMan.DefaltPosition.X, PacMan.DefaltPosition.Y));
                GameMap.AddSomeEatable(berry, berry.CurrentPosition.X, berry.CurrentPosition.Y);
                GameEventSink.Publish(new BerryAddedEvent(this, berry.CurrentPosition));
                IsBerryAdded = true;

                _berryTimer.Interval = 10000;
                _berryTimer.Elapsed += new ElapsedEventHandler(RemoveBerry);
                _berryTimer.AutoReset = false;
                _berryTimer.Start();
            }
            if (count <= 120 && !IsUnstableAdded)
            {
                IsUnstableAdded = true;
                var ghost = GameFoodContext.Ghosts[2];
                ghost.Active = true;
                _gameTimer.Interval += 20;
            }
            if (count <= 80 && !IsStupidAdded)
            {
                IsStupidAdded = true;
                var ghost = GameFoodContext.Ghosts[3];
                ghost.Active = true;
                //_gameTimer.Interval += 10;
            }
        }

        #region OldDBConnection
        public void AddScoreToDB(string name)
        {
            using (var db = new EFUnitOfWork())
            {
                var player = new Player
                {
                    Name = name,
                    Score = GameState.GetScore(),
                    Time = DateTime.Now
                };
                db.Players.Create(player);
                db.Save();
            }
        }

        public IEnumerable<PlayerDTO> GetTopTenPlayers()
        {
            using (var db = new EFUnitOfWork())
            {
                IEnumerable<Player> players = db.Players.GetTop(10);
                List<PlayerDTO> result = new List<PlayerDTO>();

                foreach (var p in players)
                {
                    var playerDTO = new PlayerDTO { Id = p.Id, Name = p.Name, Score = p.Score, Time = p.Time };
                    result.Add(playerDTO);
                }
                return result;
            }
        }
        #endregion

        public void GameRestart()
        {
            if (_isGameStarted)
            {
                UnSubscribePositionChanged();
                PacMan.Eated -= RespawnPacMan;
                _gameTimer.Elapsed -= GameTimedEvent;
                GameFoodContext.Ghosts.Clear();
            }
            _isGameStarted = true;
            GameInit();
        }

        public void GameStart()
        {
            //GameMovementTimer.InvokeTimer();

            //RunAllTimers();

            _gameTimer.Start();
        }

        public void GameStop()
        {
            //GameMovementTimer.EnableTimer = false;

            //StopAllTimers();

            _gameTimer.Stop();
        }
    }
}