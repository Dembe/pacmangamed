﻿using PacManGame.EatableModels;
using PacManGame.MainModels;

namespace PacManGame.MapBuilder
{
    public class PointFoodObject : GridObjectCreator
    {
        public PointFoodObject()
        {
            GridObject = GridObjects.PointFood;
        }

        public override IObject Create(int X, int Y)
        {
            return new PointFood(new Position(X, Y));
        }
    }
}
