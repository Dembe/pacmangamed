﻿using PacManGame.MainModels;

namespace PacManGame.MapBuilder
{
    public abstract class GridObjectCreator
    {
        public GridObjects GridObject { get; set; }
        public abstract IObject Create(int X, int Y);
    }
}
