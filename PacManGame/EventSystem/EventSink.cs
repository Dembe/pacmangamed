﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class EventSink
    {
        private readonly Dictionary<Type, List<object>> _subscribers = new Dictionary<Type, List<object>>();

        /// <summary>
        /// Invoke TEvent.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="value"></param>
        public void Publish<TEvent>(TEvent value)
        {
            // Search subscriber.
            if (_subscribers.ContainsKey(value.GetType()))
            {
                void handler(object action) => (action as Action<TEvent>)?.Invoke(value);
                _subscribers[value.GetType()].ForEach(handler);
            }
        }

        /// <summary>
        /// Subscribe on TEvent.
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="action"></param>
        public void Subscribe<TEvent>(Action<TEvent> action)
        {
            if (!_subscribers.ContainsKey(typeof(TEvent)))
            {
                _subscribers.Add(typeof(TEvent), new List<object>());
            }

            _subscribers[typeof(TEvent)].Add(action);
        }

        public void Unsubscribe<TEvent>(Action<TEvent> action)
        {
            if (!_subscribers.ContainsKey(typeof(TEvent)))
            {
                _subscribers.Remove(typeof(TEvent));
            }
        }

    }
}
