﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class BerryAddedEvent
    {
        public GameEngine CurrentGameEngine { get; private set; }

        public Position CurrentPosition { get; private set; }

        /// <summary>
        /// Adds some berry.
        /// </summary>
        /// <param name="gameEngine"></param>
        /// <param name="position"></param>
        public BerryAddedEvent(GameEngine gameEngine, Position position)
        {
            CurrentGameEngine = gameEngine;
            CurrentPosition = position;
        }
    }
}
