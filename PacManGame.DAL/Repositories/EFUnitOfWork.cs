﻿using PacManGame.DAL.EF;
using PacManGame.DAL.Entities;
using PacManGame.DAL.Interfaces;
using System;

namespace PacManGame.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly PlayerContext _db;
        private PlayerRepository _playerRepository;

        public EFUnitOfWork()
        {
            _db = new PlayerContext();
        }
        public IRepository<Player> Players
        {
            get
            {
                if (_playerRepository == null)
                    _playerRepository = new PlayerRepository(_db);
                return _playerRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
