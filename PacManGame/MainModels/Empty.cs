﻿namespace PacManGame.MainModels
{
    public class Empty : IObject, IVisited
    {
        public Position CurrentPosition { get; private set; }
        public bool IsWalkable { get; private set; }

        public Empty(Position position)
        {
            CurrentPosition = position;
            IsWalkable = true;
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public bool GetInto(FoodContext foodContext)
        {
            return false;
        }
    }
}
