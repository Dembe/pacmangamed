﻿namespace PacManGame.MainModels
{
    public class Wall : IObject, IVisited
    {
        public Position CurrentPosition { get; private set; }

        public bool IsWalkable { get; private set; }

        public Wall(Position position)
        {
            CurrentPosition = position;
            IsWalkable = false;
        }

        public bool Accept(PMan pMan)
        {
            return pMan.Visit(this);
        }

        public bool GetInto(FoodContext foodContext)
        {
            return false;
        }
    }

}
