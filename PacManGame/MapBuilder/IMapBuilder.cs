﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.MapBuilder
{
    public interface IMapBuilder
    {
        /// <summary>
        /// Returns game map (object[,]).
        /// </summary>
        /// <param name="foodContext"></param>
        object[,] BuildMap(FoodContext foodContext);
    }
}
