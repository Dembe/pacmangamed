﻿var pacManAnimId;
var GhostAnimId;

var DrawPacMan = function (curX, curY, x, y, context, directionX, directionY) {
    var imgPacMan = document.getElementById("pacManImg");

    switch (directionX) {
        case 1:
            imgPacMan.style.transform = 'rotate(0deg)';
            break;
        case -1:
            imgPacMan.style.transform = 'rotate(+180deg)';
            break;
    }
    switch (directionY) {
        case 1:
            imgPacMan.style.transform = 'rotate(+90deg)';
            break;
        case -1:
            imgPacMan.style.transform = 'rotate(-90deg)';
            break;
    }
    animatePacMan(curX, curY, x, y, context, curX * cellSize, curY * cellSize);
}
function animatePacMan(curX, curY, x, y, context, curXSize, curYSize) {

    setTimeout(function () {
        var imgPacMan = document.getElementById("pacManImg");

        pacManAnimId = requestAnimationFrame(function () { animatePacMan(curX, curY, x, y, context, curXSize, curYSize) });
        var rect = (document.getElementById("myCanvas")).getBoundingClientRect();

        var dx;
        var dy;
        if ((Math.abs(curX - x) + Math.abs(curY - y)) > 1) {
            DrawEmpty(curX, curY, context);
            imgPacMan.style.left = (x * cellSize + rect.left) + 'px';
            imgPacMan.style.top = (y * cellSize + rect.top) + 'px';

            cancelAnimationFrame(pacManAnimId);
            return;
        }
        else if (curX === x) {
            dx = 0;
            if (curY > y) {
                dy = -1;
            }
            else if (curY < y) {
                dy = 1;
            }
        }
        else if (curY === y) {
            dy = 0;
            if (curX > x) {
                dx = -1;
            }
            else if (curX < x) {
                dx = 1;
            }
        }
        Draw();
        function Draw() {
            if (!(curXSize === (x * cellSize) && curYSize === (y * cellSize))) {
                DrawEmpty(curX, curY, context);

                imgPacMan.style.left = (curXSize + rect.left) + 'px';
                imgPacMan.style.top = (curYSize + rect.top) + 'px';

                var tmp = (Math.abs(curXSize - (x * cellSize)) + Math.abs(curYSize - (y * cellSize)));
                console.log(tmp);
                if (tmp < 5) {
                    imgPacMan.src = "/Pacman_2.png";
                }
                curXSize += dx;
                curYSize += dy;
            }
            else {
                imgPacMan.src = "/Pacman.png";
                DrawEmpty(x, y, context);
                cancelAnimationFrame(pacManAnimId);
            }

        }

    }, 1000 / 100);
}

var DrawGhost = function (curX, curY, x, y, context, imgUrl, objectHtml) {
    animateGhost(curX, curY, x, y, context, imgUrl, curX * cellSize, curY * cellSize, objectHtml);
}
function animateGhost(curX, curY, x, y, context, imgUrl, curXSize, curYSize, objectHtml) {
    setTimeout(function () {
        GhostAnimId = requestAnimationFrame(function () { animateGhost(curX, curY, x, y, context, imgUrl, curXSize, curYSize, objectHtml) });
        var rect = (document.getElementById("myCanvas")).getBoundingClientRect();
        var ghostImg = document.getElementById(objectHtml);
        if (ghostImg === null) {
            alert(objectHtml);
        }
        ghostImg.src = imgUrl;

        var dx;
        var dy;
        if ((Math.abs(curX - x) + Math.abs(curY - y)) > 1) {
            DrawEmpty(curX, curY, context);
            ghostImg.style.left = (x * cellSize + rect.left) + 'px';
            ghostImg.style.top = (y * cellSize + rect.top) + 'px';
            cancelAnimationFrame(GhostAnimId);
            return;
        }
        else if (curX === x) {
            dx = 0;
            if (curY > y) {
                dy = -1;
            }
            else if (curY < y) {
                dy = 1;
            }
        }
        if (curY === y) {
            dy = 0;
            if (curX > x) {
                dx = -1;
            }
            else if (curX < x) {
                dx = 1;
            }
        }
        Draw();
        function Draw() {
            var ghostImg = document.getElementById(objectHtml);

            if (!(curXSize === (x * cellSize) && curYSize === (y * cellSize))) {
                ghostImg.style.left = (curXSize + rect.left) + 'px';
                ghostImg.style.top = (curYSize + rect.top) + 'px';

                curXSize += dx;
                curYSize += dy;
            }
            else {
                cancelAnimationFrame(GhostAnimId);
            }

        }
    }, 1000 / 100);
}

var DrawImg = function (x, y, context, path) {
    var img = new Image();
    img.src = path;
    img.onload = function () {

        context.drawImage(img, (x * cellSize), (y * cellSize), cellSize, cellSize);
    };
}

var DrawWall = function (x, y, context) {
    context.fillStyle = wallColor;
    context.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
}

var DrawPointFood = function (x, y, context) {
    requestAnimationFrame(function () {
        context.fillStyle = "black";
        context.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
        context.beginPath();
        context.arc((x * cellSize) + (cellSize / 2), (y * cellSize) + (cellSize / 2), (cellSize / 10), 0, 2 * Math.PI);
        context.fillStyle = "yellow";
        context.fill();
        context.closePath();
    });
}

var DrawEnergizer = function (x, y, context) {
    context.fillStyle = "black";
    context.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
    context.beginPath();
    context.arc((x * cellSize) + (cellSize / 2), (y * cellSize) + (cellSize / 2), (cellSize / 4), 0, 2 * Math.PI);
    context.fillStyle = energizerColor;
    context.fill();
    context.closePath();
}

var DrawEmpty = function (x, y, context) {
    context.fillStyle = "black";
    context.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
}