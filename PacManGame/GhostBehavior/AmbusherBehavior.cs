﻿using PacManGame.AStar;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;

namespace PacManGame.GhostBehavior
{
    class AmbusherBehavior : IBehavior
    {
        private readonly TeleportationPoint _tpPoint;

        public Map PacManMap { get; private set; }

        public AmbusherBehavior(Map map, TeleportationPoint teleportationPoint)
        {
            PacManMap = map;
            _tpPoint = teleportationPoint;
        }

        public List<MyPathNode> Execute(Position start, Position end)
        {
            // Convert object[,] to MyPathNode[,].
            var converter = new MyPathNodeConverter();
            MyPathNode[,] grid = converter.Convert(PacManMap.GameMap);

            Position goTo = Randomizer.GetRandPosition(PacManMap, end, 4);

            // Get shortest path from start position to 'goTo' position.
            SpatialAStar<MyPathNode, Object> spatialAStar = new SpatialAStar<MyPathNode, object>(grid, _tpPoint);
            List<MyPathNode> path = spatialAStar.Search(start, goTo, null);

            return path;
        }
    }
}
