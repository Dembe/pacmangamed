﻿using PacManGame.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.EventSystem
{
    public class PacManMoveEvent: Event
    {
        public Position PrevPosition { get; private set; }

        public GameEngine CurrentGameEngine { get; private set; }

        public PMan PacMan { get; private set; }

        public PacManMoveEvent(Position position, GameEngine gameEngine, PMan pMan)
        {
            PrevPosition = position;
            CurrentGameEngine = gameEngine;
            PacMan = pMan;
        }
    }
}
