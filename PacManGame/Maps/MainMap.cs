﻿using PacManGame.EatableModels;
using PacManGame.EventSystem;
using PacManGame.GhostBehavior;
using PacManGame.MainModels;
using PacManGame.MapBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacManGame.Maps
{
    public class MainMap : IGameMap
    {
        public int Level { get; private set; }

        public MainMap()
        {
            Level = 4;
        }

        public void GameInit(GameEngine gameEngine)
        {
            // Creating all fields.
            gameEngine.GameState = new GameState();
            gameEngine.GameEventSink = new EventSink();
            gameEngine.GameFoodContext = new FoodContext(gameEngine.GameState);
            gameEngine.GameMap = new Map(gameEngine.GameFoodContext);
            //gameEngine.GameMap.ReadMapFromXML("D:/Visual Studio/Visual Studio C# Projects/PacManGame/PacManGame.WEB/wwwroot/maps/GameMap.txt", gameEngine);
            gameEngine.GameMap.ReadMapFromTXT("h:/root/home/admindembe-001/www/site1/wwwroot/maps/GameMap.txt");
            gameEngine.GameTPPoint = new TeleportationPoint(new Position(0, 9), new Position(18, 9));
            // 9, 15
            gameEngine.PacMan = new PMan(gameEngine.GameMap, 9, 15)
            {
                GameFoodContext = gameEngine.GameFoodContext
            };
            gameEngine.PacMan.TpPoint = gameEngine.GameTPPoint;

            gameEngine.IsBerryAdded = false;
            gameEngine.IsUnstableAdded = false;
            gameEngine.IsStupidAdded = false;
            //AmbusherBehavior
            Ghost Ambusher = new Ghost(new PursuitBehavior(gameEngine.GameMap, gameEngine.GameTPPoint), gameEngine.GameMap, 6, 9, "/ghost-red.png", true);
            Ghost Chaser = new Ghost(new PursuitBehavior(gameEngine.GameMap, gameEngine.GameTPPoint), gameEngine.GameMap, 12, 9, "/ghost-blue.png", true);
            Ambusher.TpPoint = gameEngine.GameTPPoint;
            Chaser.TpPoint = gameEngine.GameTPPoint;
            gameEngine.GameFoodContext.Ghosts.Add(Ambusher);
            gameEngine.GameFoodContext.Ghosts.Add(Chaser);

            Ghost Unstable = new Ghost(new UnstableBehavior(gameEngine.GameMap, gameEngine.GameTPPoint), gameEngine.GameMap, 6, 9, "/ghost-orange.png", false);
            Ghost Stupid = new Ghost(new PursuitBehavior(gameEngine.GameMap, gameEngine.GameTPPoint), gameEngine.GameMap, 12, 9, "/ghost-pink.png", false);
            Unstable.TpPoint = gameEngine.GameTPPoint;
            Stupid.TpPoint = gameEngine.GameTPPoint;
            gameEngine.GameFoodContext.Ghosts.Add(Unstable);
            gameEngine.GameFoodContext.Ghosts.Add(Stupid);

            gameEngine.PacMan.Eated += gameEngine.RespawnPacMan;

            Energizer energizer_1 = new Energizer(new Position(1, 2));
            gameEngine.GameMap.AddSomeEatable(energizer_1, 1, 2);

            Energizer energizer_2 = new Energizer(new Position(17, 2));
            gameEngine.GameMap.AddSomeEatable(energizer_2, 17, 2);

            Energizer energizer_3 = new Energizer(new Position(1, 18));
            gameEngine.GameMap.AddSomeEatable(energizer_3, 1, 18);

            Energizer energizer_4 = new Energizer(new Position(17, 18));
            gameEngine.GameMap.AddSomeEatable(energizer_4, 17, 18);
        }
    }
}
