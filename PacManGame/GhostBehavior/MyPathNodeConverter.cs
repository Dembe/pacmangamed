﻿using PacManGame.AStar;
using PacManGame.MainModels;

namespace PacManGame.GhostBehavior
{
    public class MyPathNodeConverter
    {
        /// <summary>
        /// Converts object[,] map to MyPathNode[,] map.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public MyPathNode[,] Convert(object[,] map)
        {
            int width = map.GetLength(0);
            int height = map.GetLength(1);

            MyPathNode[,] grid = new MyPathNode[width, height];

            bool isWall;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (map[x, y] is Wall)
                        isWall = true;
                    else
                        isWall = false;

                    grid[x, y] = new MyPathNode()
                    {
                        IsWall = isWall,
                        X = x,
                        Y = y,
                    };
                }
            }

            return grid;
        }
    }
}
