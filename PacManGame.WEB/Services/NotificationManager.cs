﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PacManGame.EatableModels;
using PacManGame.EventSystem;
using PacManGame.Maps;

namespace PacManGame.WEB
{
    public class NotificationManager
    {
        private readonly IHubContext<GameHub> _hubContext;
        private readonly GameEngineList _gameEngineList;

        public NotificationManager(IHubContext<GameHub> gameHub, GameEngineList gameEngineList)
        {
            _hubContext = gameHub;
            _gameEngineList = gameEngineList;
        }

        public async void Handle(PacManMoveEvent pacManMoveEvent)
        {
            string connID = _gameEngineList.GetConnID(pacManMoveEvent.CurrentGameEngine);
            if (connID == null)
                return;
            await _hubContext.Clients.Client(connID).SendAsync("PrintPacMan", 
                pacManMoveEvent.PrevPosition.X, pacManMoveEvent.PrevPosition.Y,
                pacManMoveEvent.PacMan.CurrentPosition.X, pacManMoveEvent.PacMan.CurrentPosition.Y,
                pacManMoveEvent.PacMan.PacManDirection.X, pacManMoveEvent.PacMan.PacManDirection.Y);
        }

        public async void Handle(GhostMoveEvent ghostMoveEvent)
        {
            string connID = _gameEngineList.GetConnID(ghostMoveEvent.CurrentGameEngine);
            if (connID == null)
                return;

            string img = "";

            Ghost ghost = ghostMoveEvent.CurrentGhost;

            if (ghost.IsDead)
            {
                img = "/ghost-dead.png";
            }
            else if (ghost.IsFrightened)
            {
                img = "/ghost-chase.png";
            }
            else
            {
                img = ghost.ImgUrl;
            }

            await _hubContext.Clients.Client(connID).SendAsync("PrintGhost",
                ghostMoveEvent.PrevPosition.X, ghostMoveEvent.PrevPosition.Y,
                ghost.CurrentPosition.X, ghost.CurrentPosition.Y,
                img, ghostMoveEvent.CurrentGameEngine.GameFoodContext.Ghosts.IndexOf(ghost));
        }

        public async void Handle(RespawnEvent respawnEvent)
        {
            string connID = _gameEngineList.GetConnID(respawnEvent.CurrentGameEngine);
            if (connID == null)
                return;

            await _hubContext.Clients.Client(connID).SendAsync("UseLife", respawnEvent.CurrentGameEngine.GameState.GetAmountOfLives());
        }

        public async void Handle(GhostAddedEvent ghostAddedEvent)
        {
            string connID = _gameEngineList.GetConnID(ghostAddedEvent.CurrentGameEngine);
            if (connID == null)
                return;
            await _hubContext.Clients.Client(connID).SendAsync("AddGhost", ghostAddedEvent.CurrentPosition.X, ghostAddedEvent.CurrentPosition.Y, ghostAddedEvent.CurrentGameEngine.GameFoodContext.Ghosts.Count);
        }

        public async void Handle(BerryAddedEvent berryAddedEvent)
        {
            string connID = _gameEngineList.GetConnID(berryAddedEvent.CurrentGameEngine);
            if (connID == null)
                return;
            await _hubContext.Clients.Client(connID).SendAsync("PrintBerry", berryAddedEvent.CurrentPosition.X, berryAddedEvent.CurrentPosition.Y);
        }

        public async void Handle(GameOverEvent gameOverEvent)
        {
            string connID = _gameEngineList.GetConnID(gameOverEvent.CurrentGameEngine);
            if (connID == null)
                return;
            await _hubContext.Clients.Client(connID).SendAsync("GameOver", gameOverEvent.CurrentGameEngine.GameState.GetScore());
        }

        public async void Handle(NextLevelEvent nextLevelEvent)
        {
            string connID = _gameEngineList.GetConnID(nextLevelEvent.CurrentGameEngine);
            if (connID == null)
                return;

            await _hubContext.Clients.Client(connID).SendAsync("NextLevel" , nextLevelEvent.GameLvl, nextLevelEvent.Score);
        }

        public async void Handle(RemoveObjectEvent removeObjectEvent)
        {
            string connID = _gameEngineList.GetConnID(removeObjectEvent.CurrentGameEngine);
            if (connID == null)
                return;

            await _hubContext.Clients.Client(connID).SendAsync("RemoveObject", removeObjectEvent.ObjectPosition.X, removeObjectEvent.ObjectPosition.Y);
        }
    }
}
