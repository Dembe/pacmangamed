﻿using PacManGame.AStar;
using PacManGame.MainModels;
using System.Collections.Generic;

namespace PacManGame.GhostBehavior
{
    public interface IBehavior
    {
        Map PacManMap { get; }

        /// <summary>
        /// Returns path (List'MyPathNode') according to current behavior from start point to end point.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        List<MyPathNode> Execute(Position start, Position end);
    }
}
